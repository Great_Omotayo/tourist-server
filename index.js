const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');
const config = require('./config/database');
const passport =  require('passport');
const app = express();
app.use(fileUpload());
app.use(express.static(__dirname + '/dist'));
mongoose.connect(config.database);
// var conn = mongoose.connection;
// conn.on('error', console.error.bind(console, 'connection error:'));
// conn.once('open', function () {console.log("Great success!")});
 mongoose.connection.on("connected", function(){
    console.log("Connection Estabilished")
});
app.use(bodyparser.json());
app.use(passport.initialize());
app.use(passport.session());
app.use(cors());
require('./config/passport');
const routes = require('./routes');
app.use('/',routes);
app.listen(process.env.PORT || 8080);
console.log(__dirname);