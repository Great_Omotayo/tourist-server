var express = require('express');
var fs = require('fs');
const path = require('path');
var bcrypt = require('bcryptjs');
var router = express.Router();
var add = require('./models/add/insert.js');
var select = require('./models/select/select.js');
const config = require('./config/database');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var handle = require('./handle-images');

//Add attraction to the list of searchable 
router.post('/addAttraction',function(req,res){
   add.addAttraction(req.body.data,req.files,function(err,newcentre){
   	if(err){
   		console.log(err);
   		return res.json({'status':'failed','message':'Please try again later'});
   	}
   	if(newcentre){
      console.log(newcentre);
   	   res.json({'status':'success','message':'New tourist centre added','res':newcentre});
   	}
   })
});
//Users can comment about a tourist centre
//each comments are saved within the attraction json
router.post('/comments/:id', function(req,res){
	//return console.log(req.body);
   add.addComment(req.params.id,req.body,function(err,attr){
   	if(err){
   		return res.json({'status':'failed','message':'Please try again later'});
   	}
   	if(attr){
   	   res.json({'status':'success','message':'Comment saved','res':attr});
   	}
   })
});
//Admin can update his facilities by adding to the existing list
router.post('/addfacilities/:id', function(req,res){
	//return console.log(req.body);
   add.addfacilities(req.params.id,req.body,function(err,attr){
   	if(err){
   		return res.json({'status':'failed','message':'Please try again later'});
   	}
   	if(attr){
   	   res.json({'status':'success','message':'Facilities saved','res':attr});
   	}
   })
});
//save book to DB
 router.post('/savebook',function(req,res){
  //console.log(req.body);
   add.NewBook(req.body,function(err,newbook){
     if(err)
      return res.json({'status':'failed','message':'Please try again later'});        
     if(newbook){
      console.log(newbook);
       return res.json({'status':'success','message':'new book added','res':newbook})      
     }
   }); 
 });
 //The admin ca change the image of the attraction, if he likes.
 router.post('/changeImage/:id',function(req,res){	
   savedImages = saveImages(req.files);	
   add.changeimage(req.params.id,req.body.removeimageindex,savedImages,function(err,attr){
   	if(err){
   		return res.json({'status':'failed','message':'Please try again later'});
   	}
   	else{
   	   res.json({'status':'success','message':'Update received','res':attr});
   	}
   })
 });
 //search for attractions
 router.post('/getAttractions',function(req,res){
   select.GetAttraction(req.body,function(err,attr){
   	 if(err){
   	 	return res.json({'status':'failed','message':'Please try again later'});
   	 }
   	  res.json({'status':'success','message':'','res':attr});

   })
 });
 //get a particular attraction form the DB
 router.post('/getAttraction/:id',function(req,res){
   select.getAttractionById(req.params.id,function(err,attr){
   	 if(err){
   	 	return res.json({'status':'failed','message':'Please try again later'});
   	 }
   	  res.json({'status':'success','message':'Update received','res':attr});

   })
 });
 router.post('/getAttraction2',function(req,res){
   select.GetAttractione(req.body,function(err,attr){
   	 if(err){
   	 	return res.json({'status':'failed','message':'Please try again later'});
   	 }
   	  res.json({'status':'success','message':'Search results','res':attr});

   })
 });
 //Get all booked for the admin
 router.post('/getBooked/:id',function(req,res){
   select.getBooked(req.params.id,function(err,booked){
   	if(err){
   		return res.json({'status':'failed','message':'Please try again later'});
   	}
   	else{
   	   res.json({'status':'success','message':'Update received','res':booked});
   	}
   })
 });
 //Get attraction profile for edit
 router.post('/getProfile/:id',function(req,res){
   select.getProfile(req.params.id,function(err,profile){
   	if(err){
   		return res.json({'status':'failed','message':'Please try again later'});
   	}
   	else{
   	   res.json({'status':'success','message':'Update received','res':profile});
   	}
   })
 });
 //Once the admin does editing of profile save the this to the DB
  router.post('/editedProfile/:id', function(req,res){
    console.log(req.body);
    return  res.json({'status':'success','message':'Update received','res':'editted'});
    add.EditedProfile(req.params.id,req.body,function(err,editted){
    	if(err){
    		return res.json({'status':'failed','message':'Please try again later'});
    	}else{
    	  res.json({'status':'success','message':'Update received','res':editted});
    	}
    })
  });
 //Take care of logging in the admin to this account
 router.post('/loggIn',function(req,res){    
  //console.log(req.body);
 	select.loggIn(req.body,function(err,attr){
       if(err){
       	return res.json({'status':'failed','message':'Please try again later'});
       }
       if(!attr){
       	return res.json({'status':'failed','message':1});
       }
       	bcrypt.compare(req.body.password, attr.admin.password,function(err,status){
       		if(status !== true){
       			return res.json({'status':'success','message':0,'res':status});       	
       		}
       		secret = config.secret;
       		var payload = {adminemail:attr.admin.email,adminpassword:req.body.password};
       		const token = jwt.sign(payload,secret,{expiresIn: 64800});
          response = {success:true, token: 'JWT ' +token,  result:
     	        {
     		          id: attr._id,
     		          email:attr.admin.email,
                  attraction:attr
     	        }
       		}
          select.getBooked(attr._id,function(err,booked){
            if(err) 
              return res.json({'status':'failed','message':'Please try again later'});
          //console.log(booked);
          res.json({'status':'success','message':1,'res':response,'booked':booked});      

            //else if(!booked)
              //res.json({'status':'success','message':1,'res':response,'booked':booked});      
          })
        });  
 	});
 });
 router.get('/gate',function(req,res){
   select.getBook(function(err,booked){
      if(booked)
        res.json(booked);
   })
 });
 router.post('/upload',function(req,res){
  if(req.files.photo instanceof Array){
    var ff = handle.uploadImage(req.files.photo);
  }else{
   var ff = handle.singleuploadImage(req.files.photo);
  } 
  
 });
 //Take care of recommendation to the user

 //Handle image uploads
   function saveImages(files){
     imageuploaded = 0;
     images = [];
     for (i=0;i<files.length;i++){
     	rename = Math.floor(Math.random() * 1000000000);
 	    ext = files.image.name.split('.')[1];
        files.image.mv(__dirname + '/dist/images/'+rename+'.'+ext+'',function(err){
   	    if(err) console.log(err);
   	    else{
          images.push(rename+'.'+ext);
          imageuploaded++;
   	    }
   	    return images;
        });
     }	 
  }
  //delete file
	  function deleteImage(deleteImg){
	  	for (i=0;i<deleteImg.length;i++){
		  	fs.unlink(__dirname + '/dist/images/'+deleteImg[i]+'', function(error) {
		      if (error) {}
		    });
	    }
	  }
router.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname + '/dist/index.html'));
});  
module.exports = router;