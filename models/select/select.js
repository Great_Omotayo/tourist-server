//select from file
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Attraction = require('../attrschema');
var User = require('../userschema');
var Book = require('../bookschema');
module.exports.GetAttraction = function(body,callback){
	console.log(body);
	price = body.max_price != 0 ? body.max_price : 500000; 
	if(body.location !='' && body.type !=''){
		Attraction.find({AttractionLocation:body.location,AttractionCategory:body.type}).where('Priceperhead').lte(price).exec(callback);
	}else if(body.location !='' && body.type ==''){
	    Attraction.find({AttractionLocation:body.location}).where('Priceperhead').lte(price).exec(callback);	
	}else if(body.location =='' && body.type !=''){
		console.log("am");
	    Attraction.find({AttractionCategory:body.type}).where('Priceperhead').lte(price).exec(callback);			
	}else if(body.location =='' && body.type ==''){
	    Attraction.find({}).where('Priceperhead').lte(price).exec(callback);					
	}
}
module.exports.ExistingUser = function(email,callback){
    Attraction.findOne({email:email},callback);
}
module.exports.getBooked = function(id,callback){
    Book.find({attraction_id:id},callback);
}
module.exports.getBook = function(callback){
    Book.find(callback);
}
module.exports.getProfile = function(id,callback){
    Book.findById(id,callback);
}
module.exports.getAttractionById = function(id,callback){
    Attraction.findById(id,callback);
}
module.exports.GetAttractione = function(body,callback){
    Attraction.find(callback);
}
module.exports.loggIn = function(body,callback){
    Attraction.findOne({'admin.email':body.email},function(err,attr){
    	if(err){
    		callback(err,null);
    	}else{
    		callback(null,attr);
    	}
    });
	
}
module.exports.ConfirmAttr = function(body,callback){
	Attraction.findOne({'admin.email':body.email},function(err,attr){
    	if(err){
    		callback(err,null);
    	}else if(attr){
          bcrypt.compare(body.password, attr.admin.password,callback);
    	}

    });
}