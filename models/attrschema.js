//Add new tourist centre
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
//Attraction Schema
const AttractionSchema = mongoose.Schema({
	AttractionName:{
		type:String,
		require: true
	},
	AttractionEmail:{
		type:String,
		require:true
	},
	AttractionPhone:{
		type:Number,
		require: true
	},
	AttractionLocation:{
		type:String,
		require: true
	},
	AttractionTown:{
		type:String,
		require: true
	},
	AttractionCategory:{
		type:String,
		require: true
	},
	AttractionAddress:{
		type:String,
		require: true
	},
	drivingInstruction:{
		type:String,
	},
	admin:{
		username:{
			type:String,require:true
		},
		email:{
			type:String,require:true
		},
		phone:{
			type:Number,require:true
		},
		password:{
			type:String,require:true
		}
	},
	Priceperhead:{
        type:Number,
        require: true
    },
	Ratings: Number,
	Likes: Number,
	Facilities: [],
	Images:[],
	comments:[],
	create_date:{
		type:Date,
		default:Date.now
	}
});
var Attraction = module.exports =  mongoose.model('Attractions',AttractionSchema);