var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
//User Schema
var UserSchema = mongoose.Schema({
    Firstname: {
    	type:String,
    	require:true
    },
    Lastname: {
    	type:String,
    	require:true
    },
    email:{
        type:String,
    	require:true
    },
    phone:{
        type:Number,
    	require:true
    },
    Booking:[],
    create_date:{
    	type:Date,
    	default:Date.now
    }
});
var User = module.exports = mongoose.model('User',UserSchema);