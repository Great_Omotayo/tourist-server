var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Attraction = require('../attrschema');
var User = require('../userschema');
var Book = require('../bookschema');
var handle = require('../../handle-images');

module.exports.addUser = function(body,callback){
	//console.log(body);
    newUser = new User(body.user);
    newUser.save(callback);
}
module.exports.NewBook = function(body,callback){
	newBook = new Book(body);
	newBook.save(callback);
}
module.exports.addAttraction = function(body,files,callback){
	//console.log(body);return;
	bcrypt.genSalt(10, function(err, salt) {
	modifybody = JSON.parse(body);
    bcrypt.hash(modifybody.admin.password, salt, function(err, hash) {
        var savedimg;
		  if(files.photo instanceof Array){
		    savedimg = handle.uploadImage(files.photo);
		  }else{
		    savedimg = handle.singleuploadImage(files.photo);
		  }
		objbody = JSON.parse(body);  
		objbody.Images.push.apply(objbody.Images, savedimg);  
		objbody.admin.password = hash;
        //var strbody = JSON.stringify(objbody);
        //console.log(strbody);return;
        newattraction = new Attraction(objbody);
        newattraction.save(callback);
    });
});
    
}
module.exports.addComment = function(id,comment,callback){
	//return console.log(comment);
    Attraction.findById(id, function(err,attr){
    	if(err) return callback(err,null);
    	//return console.log(attr[0].AttractionName);
        attr.comments.push(comment);
        attr.save(callback);
    });
}
module.exports.addfacilities = function(id,facilities,callback){
	//return console.log(id);
    Attraction.findById(id, function(err,attr){
    	if(err) return callback(err,null);
        for(var i = 0; i < facilities.length; i++) 
	    {
		  if (attr.Facilities.indexOf(facilities[i]) == -1) {
		  	console.log(facilities[i]);
		  	attr.Facilities.push(facilities[i]);
	       }
		}  	
		//attr.Facilities = [];
        attr.save(callback);
    });
}
module.exports.changeimage = function(id,removeimagesindex,addimages,callback){
	Attraction.findById(id, function(err,attr){
		if(err) return callback(err,null);
		for(i=0;i<removeimagesindex.length;i++){
        	if(attr.Images.splice(removeimagesindex[i],1));
        }
        for(i=0;i<addimages.length;i++){
        	if(attr.Images.push(addimages[i]));
        }
        attr.save(callback);
	});
}
module.exports.EditedProfile = function(id,body,callback){
	Attraction.findById(id,function(err,attr){
		if(err){
			return callback(err,null);
		}else{
			attr.AttractionName = body.AttractionName || attr.AttractionName;
			attr.AttractionEmail = body.AttractionEmail || attr.AttractionEmail;
			attr.AttractionPhone = body.AttractionPhone || attr.AttractionPhone;
			attr.AttractionLocation = body.AttractionLocation || attr.AttractionLocation;
			//attr.AttractionTown = body.AttractionTown || attr.AttractionTown;
			attr.AttractionCategory = body.AttractionCategory || attr.AttractionCategory;
			attr.AttractionAddress = body.AttractionAddress || attr.AttractionAddress;
		    attr.Priceperhead = body.Priceperhead || attr.Priceperhead;
		    attr.drivingInstruction = body.drivingInstruction || attr.drivingInstruction;
		    attr.admin.username = body.username || attr.admin.username;
		    attr.admin.email = body.email || attr.admin.email;
		    attr.admin.phone = body.phone || attr.phone;
		    attr.save(callback);
         }
	})
}

