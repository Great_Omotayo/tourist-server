var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
//Booking
var BookSchema = mongoose.Schema({
    attraction_id: {
    	type:String,
    	require:true
    },
    amount_paid:{
    	type:Number,
    	require:true
    },
    modeOfPayment:{
        type:String,
        require:true
    },
    user:{
        name:{
            type:String
        },
        email:{
            type:String,require:true
        },
        phone:{
            type:String
        }
    },
    tour_date: String,
    transaction_details:String,
    tourist: Number,
    cancel:Boolean,
    create_date:{
    	type:Date,
    	default:Date.now
    }
});
var Book = module.exports = mongoose.model('Books',BookSchema);